long[] timings   = {10,4};//{100,100,100,100,100,100,100,100,400,400};
int[] amplitudes = {0,255};//{ 63,127,191,255,191,127, 63,  0,127,  0};

//  11  09     06  04  02
//12  10  08 07  05  03  01
//  --  --    10  --  --    -3  01
//--  --  ----  08  --  0403  02  -1
int[][] map = {
  {30,8},
  {60,4},
  {90,2},
  {120,6}
};

int keyWidth;
int keyMax = 18;

void setup(){
   orientation(LANDSCAPE);
   keyWidth = width / keyMax;
   
   for(int i=0;i<keyMax;i++){
    fill(255);
    rect(keyWidth * i,0,keyWidth,height);
    
    fill(0);
    textSize(50);
    text(i+1, (keyWidth*i)+40,height-200);
    text(1000/(i+10),(keyWidth*i)+40,height-100);
    println((float)1000/(i+10));
  }
  
}
void draw(){
  noStroke();
  fill(0,255,255);
  ellipse (mouseX,mouseY,30,30);
}

void mousePressed(){
  for(int i=0;i<keyMax;i++){
    if(keyWidth*i < mouseX && mouseX < keyWidth*(i+1)){
      vibChange(i+1);
    }
  }
}
void mouseReleased(){
  stopVibrate();
}

void vibChange(int n){
  if(n==0)stopVibrate();
  timings[0] = n+8;//test
  timings[1] = 1;
  vibrate(timings, amplitudes,0);
}
